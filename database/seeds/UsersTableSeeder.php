<?php

use Illuminate\Database\Seeder;
use app\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $adminUser = [
            'name' => 'Admin',
            'email' => 'irh@gmail.com',
            'password' => 123456789('admin'),

        ];
        if (!User::where('email', @adminUser['email'])->exist()){
            User::create($adminUser);
        }
    }
}
